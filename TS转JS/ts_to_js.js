/*
 * @Author: xiaosihan 
 * @Date: 2023-04-30 21:59:33 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-04-30 22:11:23
 */
const { exec } = require('child_process');
const fs = require('fs');
const path = require('path');

const tsconfig = {
    "compilerOptions": {
        "target": "esnext",
        "lib": [
            "dom",
            "dom.iterable",
            "esnext"
        ],
        "types": [
            "vite/client",
            "node"
        ],
        "typeRoots": [
            "./",
        ],
        "sourceMap": false,
        "declaration": false,
        "allowJs": true,
        "skipLibCheck": true,
        "esModuleInterop": true,
        "allowSyntheticDefaultImports": true,
        "strict": false,
        "forceConsistentCasingInFileNames": true,
        "module": "esnext",
        "moduleResolution": "node",
        "resolveJsonModule": true,
        "isolatedModules": false,
        "noEmit": false,
        "noImplicitAny": false,
        "locale": "zh-cn",
        "jsx": "preserve",
    },
    "include": [
        "./",
    ],
    "exclude": []
}

const jsonData = JSON.stringify(tsconfig);

(async () => {

    fs.writeFileSync('./tsconfig.json', jsonData);
    
    await new Promise((resolve, reject) => {
        exec('npx tsc', { cwd: "./" }, (err, stdout, stderr) => {
            resolve();
        });
    });
    
    fs.rmSync('./tsconfig.json');

})();
