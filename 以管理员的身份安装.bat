
title 前端小工具安装中.....;

set current_dir=%~dp0

reg add "HKEY_CLASSES_ROOT\Directory\Background\shell\前端小工具" /v "Icon"  /t "REG_SZ"  /d "%current_dir%webTool.ico" /f

reg add "HKEY_CLASSES_ROOT\Directory\Background\shell\前端小工具" /v "SubCommands"  /t "REG_SZ"  /d "创建项目;优化图片;取色工具;截动态屏;端口占用清理;TS转JS;获取局域网ip;更新前端小工具" /f


reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\创建项目" /v "Icon"  /t "REG_SZ"  /d "%current_dir%创建项目\creatProject.ico" /f

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\创建项目\command" /v ""  /t "REG_EXPAND_SZ"  /d "%current_dir%创建项目\creatProject.bat %%V " /f


reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\优化图片" /v "Icon"  /t "REG_SZ"  /d "%current_dir%优化图片\tinypngFs.ico" /f

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\优化图片\command" /v ""  /t "REG_EXPAND_SZ"  /d "%current_dir%优化图片\tinypngFs.bat %%V " /f


reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\取色工具" /v "Icon"  /t "REG_SZ"  /d "%current_dir%取色工具\ColorPix.ico" /f

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\取色工具\command" /v ""  /t "REG_EXPAND_SZ"  /d "%current_dir%取色工具\ColorPix.exe" /f

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\截动态屏" /v "Icon"  /t "REG_SZ"  /d "%current_dir%截动态屏\ScreenToGif.ico" /f

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\截动态屏\command" /v ""  /t "REG_EXPAND_SZ"  /d "%current_dir%截动态屏\ScreenToGif.exe" /f


reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\TS转JS" /v "Icon"  /t "REG_SZ"  /d "%current_dir%TS转JS\ts_to_js.ico" /f

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\TS转JS\command" /v ""  /t "REG_EXPAND_SZ"  /d "%current_dir%TS转JS\ts_to_js.bat %%V " /f


reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\端口占用清理" /v "Icon"  /t "REG_SZ"  /d "%current_dir%端口占用清理\clearPort.ico" /f

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\端口占用清理\command" /v ""  /t "REG_EXPAND_SZ"  /d "%current_dir%端口占用清理\clearPort.bat %%V " /f


reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\获取局域网ip" /v "Icon"  /t "REG_SZ"  /d "%current_dir%获取局域网ip\getIP.ico" /f

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\获取局域网ip\command" /v ""  /t "REG_EXPAND_SZ"  /d "%current_dir%获取局域网ip\getIP.bat %%V " /f


reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\更新前端小工具" /v "Icon"  /t "REG_SZ"  /d "%current_dir%更新前端小工具\update.ico" /f

reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\更新前端小工具\command" /v ""  /t "REG_EXPAND_SZ"  /d "%cd%更新前端小工具\update.bat" /f
