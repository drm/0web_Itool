
项目 说明:

技术结构: webpack + react + less + typescript + mobx + antd

安装包: npm i

npm start :  启动项目

npm run build : 打包项目

目录结构

./webpack         webpack的配置

./src             项目目录

./src/index.tsx   项目入口文件

./src/Routers.tsx 路由配置

./src/reset.less  通用css样式初始化

./src/index.html  主页html

./src/components  所有的公共组件

./src/fonts       字体文件目录

./src/images      公共图片路径

./src/pages       所有的页面

./src/service     所有的请求

./src/store       mobx全局监听对象

./src/utils       公用的方法

./src/worker      多线程文件









