/*
 * @Author: 肖思汗 
 * @Date: 2019-08-18 13:27:09 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-04-10 15:40:41
 */
//
// 异常页面
import loadable from '@loadable/component';
// 布局
import Layout from '@pages/layout/Layout';
import ReactMixinComponent from '@ReactMixinComponent';
import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';

const Error404 = loadable(() => import('@pages/404/Error404')); // 404

const Dev = loadable(() => import('@pages/Dev/Dev'));

// 主页
const Home = loadable(() => import('@pages/home/Home'));

interface Props {
}

interface State {
}

export default class Routers extends ReactMixinComponent<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <Layout>
                <Router history={this.mixin_history}>
                    <Switch>
                        <Route exact path={["/", "/home"]} component={Home} />
                        <Route exact path="/dev" component={Dev} />
                        <Route path="*" component={Error404} />
                    </Switch>
                </Router>
            </Layout>
        );
    }
}