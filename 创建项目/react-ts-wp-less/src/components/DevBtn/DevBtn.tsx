/*
 * @Author: xiaosihan
 * @Date: 2021-03-28 18:50:42
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-07-10 20:04:49
 */

import globalStore from '@globalStore';
import ReactMixinComponent from '@ReactMixinComponent';
import { Switch } from 'antd';
import * as React from 'react';
import styles from './DevBtn.less';

interface Props { }

interface State {
    isDev: boolean
}

export class DevBtn extends ReactMixinComponent<Props, State> {
    constructor(props: Props) {
        super(props);

    }

    state: State = {
        isDev: globalStore.isDev
    }

    componentDidMount() {
        this.mixin_autorun(() => {
            const { isDev } = globalStore;
            this.setState({ isDev });
        });
    }

    render() {
        const { isDev } = this.state;

        return (
            <div className={styles.DevBtn} >
                开发模式 : &nbsp;
                <Switch
                    className={styles.switch}
                    checked={isDev}
                    onChange={isDev => {
                        globalStore.isDev = isDev;
                    }}
                />
            </div>
        );
    }
}