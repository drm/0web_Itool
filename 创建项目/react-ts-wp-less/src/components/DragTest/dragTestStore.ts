/*
 * @Author: xiaosihan
 * @Date: 2021-03-28 16:54:59
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-03-28 18:34:26
 */
import { observable } from 'mobx';

// 全局状态
const dragTestStroe = observable({
    top: 0,
    left: 0
});

export default dragTestStroe;