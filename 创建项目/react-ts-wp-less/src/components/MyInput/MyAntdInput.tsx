/*
 * @Author: 肖思汗
 * @Date: 2020-03-20 16:43:46
 * @Last Modified by: 肖思汗
 * @Last Modified time: 2020-08-16 22:53:08
 */
// react tsx 组件例子
import React from 'react';
import styles from './MyInput.less';
import MyInput, { MyInputProps } from './MyInput';
import { Input } from 'antd';

export interface MyAntdInputProps extends MyInputProps {
    size?: any
}


interface IState { }

/**
 *
 *
 * @export
 * @class MyAntdInput
 * @extends {MyInput}
 */
export default class MyAntdInput extends MyInput<MyAntdInputProps> {

    constructor(props: Readonly<MyAntdInputProps>) {
        super(props);
    }

    render() {

        const { onInput, onChange, className, value, defaultValue, size, ...other } = this.props;

        return (
            <Input
                className={`${className} ${styles.MyInput}`}
                ref={Input => {
                    if (Input) {
                        this.input = Input.input;
                        this.input.value = this.verify(String(value || defaultValue || ''))
                    }
                }
                }
                onInput={this.onInput}
                onPaste={this.onPaste}
                onCompositionStart={this.onCompositionStart}
                onCompositionEnd={this.onCompositionEnd}
                size={size}
                {...other}
            />
        )
    }
}
