/*
 * @Author: 肖思汗
 * @Date: 2020-03-20 16:43:46
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-07-10 20:10:10
 */
// react tsx 组件例子
import React from 'react';
import styles from './ReactComponent.less';
import ReactMixinComponent from '@ReactMixinComponent';
import globalStore from '@globalStore';

/**
 * @class ReactComponentTsx组件模版 16版
 * @extends {Component}
 */

interface IProps {
}

interface IState {
    testValue1: number;
    testValue2: number;
}

export default class ReactComponentTs extends ReactMixinComponent<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
    }

    state = {
        testValue1: globalStore.testValue1,
        testValue2: globalStore.testValue2
    }

    // react 16版推荐使用的5个生命周期
    componentDidMount() {

        // 监听 globalStore.testValue 的变化
        this.mixin_autorun(() => {
            const { testValue1, testValue2 } = globalStore;
            this.setState({ testValue1, testValue2 });
        });
    }

    componentDidUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, snapshot: string) {
    }

    // 不写这个生命周期的话 会使用 ReactMixinComponent 中的方法进行简易的判断
    shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any) {
        return super.shouldComponentUpdate(nextProps, nextState, nextContext);
    }

    componentDidCatch(error: any, errorInfo: any) { }

    componentWillUnmount() { }

    render() {

        const { testValue1, testValue2 } = this.state;

        return (
            <div className={styles.ReactComponent} >
                <div>globalStore.testValue1 : {testValue1}</div>
                <div>globalStore.testValue2 : {testValue2}</div>
            </div>
        )
    }
}
