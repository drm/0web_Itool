import React from 'react';
import styles from './btn1.less';
import ReactMixinComponent from '@ReactMixinComponent';

interface Iprops {
    className?: string;
    onClick?: () => void;
}

interface Istate {

}

export default class Btn1 extends ReactMixinComponent<Iprops, Istate> {

    constructor(props: Iprops) {
        super(props);
    }

    render() {
        return (
            <span {...this.props} className={`${styles.btn1} ${this.props.className || ''}`}>
                <span className={styles.before} />
                {this.props.children || '按钮1'}
                <span className={styles.after} />
            </span>
        );
    }
}
