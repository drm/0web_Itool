import { Button } from 'antd';
import React from 'react';
import styles from './btn2.less';
import ReactMixinComponent from '@ReactMixinComponent';

interface Iprops {
    className?: string
}

interface Istate {

}

export default class Btn2 extends ReactMixinComponent<Iprops, Istate> {
    render() {
        return (
            <Button {...this.props} className={`${styles.btn2} ${this.props.className || ''}`}>
                <span className={styles.before} />
                &nbsp; &nbsp;
                {this.props.children || '按钮2'}
                &nbsp; &nbsp;
                <span className={styles.after} />
            </Button>
        );
    }
}
