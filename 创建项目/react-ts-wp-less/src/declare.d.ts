

// 声明模块
declare module '*.less'
declare module '*.svg'
declare module '*.png'
declare module '*.jpg'
declare module '*.jpeg'
declare module '*.gif'
declare module '*.bmp'
declare module '*.tiff'
declare module '*.obj'
declare module '*.glb'
declare module '*.gltf'
declare module '*.xlsx'
declare module '*.xls'
declare module '*.pdf'
declare module '*.mp3'
declare module '*.mp4'
declare module 'get-browser-info';


declare const _DEV_: boolean;

declare interface Window {
    globalStore: any
}