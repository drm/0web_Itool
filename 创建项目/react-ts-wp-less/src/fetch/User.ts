/*
 * @Author: 肖思汗
 * @Date: 2020-08-01 00:02:59
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-03-28 18:46:14
 */

import Fetch from "./fetch";


class User extends Fetch {
    constructor() {
        super();
    }

    async login({ userName, password }: { userName: string, password: string }) {

        let { res, err } = await this.request({
            method: 'post',
            url: '',
            data: { userName, password },
            params: { userName, password },
            headers: {},
        });

        console.log('res', res);
        console.log('err', err);
    }

}

const user = new User();

export default user;
