import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import _ from 'lodash';

export interface FetchRequestConfig extends AxiosRequestConfig {
    successTip?: boolean, // 请求成功提示
    errorTip?: boolean, // 网络错误提示
}

// 发送请求的基类 
export default class Fetch {

    constructor() { }

    // 设置响应拦截器
    private setResponseInterceptor(axios: AxiosInstance) {
        axios.interceptors.response.use(
            response => { // 对响应数据做点什么
                return response
            },
            err => {  // 对响应错误做点什么
                return Promise.reject(err);
            }
        );

        return axios;
    }

    // 设置请求拦截器
    private setRequestInterceptor(axios: AxiosInstance) {
        // 添加拦截器
        axios.interceptors.request.use(
            config => { // 在发送请求之前做些什么
                return config;
            },
            err => { // 网络异常 要做什么
                return Promise.reject(err);
            }
        );

        return axios;
    }

    private defaultParam: FetchRequestConfig = { // 默认的请求参数
        method: 'post',
        timeout: 1 * 60 * 1000,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        baseURL: '/',
        url: '/defalutUrl'
    }

    // 创建 axios 实例
    private creatAxios() {
        return Axios.create();
    }

    // 发送请求
    async request(parameters: FetchRequestConfig) {

        // 组装 axios 需要的参数 与默认的参数加在一起
        let axiosRequestConfig = _.merge(this.defaultParam, parameters);

        // 创建请求实例
        let axios = this.creatAxios();

        // 设置请求拦截器
        axios = this.setRequestInterceptor(axios);

        // 设置响应拦截器
        axios = this.setResponseInterceptor(axios);

        let result: { res?: any, err?: any } = {};

        // 发送请求
        await axios(axiosRequestConfig)
            .then(res => {
                result = { res };
            })
            .catch(err => {
                result = { err };
            });

        return result;
    }

}