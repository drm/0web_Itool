/*
 * @Author: xiaosihan 
 * @Date: 2021-03-28 02:25:11 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-07-10 20:13:33
 */

import getBrowserInfo from 'get-browser-info';
import { configure, observable } from 'mobx';

// mobx 配置
configure({
    enforceActions: 'never' // 允许直接修改状态
})

// 浏览器 列表
//browser: ['Safari', 'Chrome', 'Edge', 'IE', 'Firefox', 'Firefox Focus', 'Chromium', 'Opera', 'Vivaldi', 'Yandex', 'Arora', 'Lunascape', 'QupZilla', 'Coc Coc', 'Kindle', 'Iceweasel', 'Konqueror', 'Iceape', 'SeaMonkey', 'Epiphany', '360', '360SE', '360EE', 'UC', 'QQBrowser', 'QQ', 'Baidu', 'Maxthon', 'Sogou', 'LBBROWSER', '2345Explorer', 'TheWorld', 'XiaoMi', 'Quark', 'Qiyu', 'Wechat', 'Taobao', 'Alipay', 'Weibo', 'Douban', 'Suning', 'iQiYi'],
// browserInfo = {
// browser: "Chrome"
// browserVersion: "83.0.4103.61"
// device: "PC"
// engine: "Blink"
// language: "zh_CN"
// os: "Windows"
// osVersion: "10.0"
// }

// 全局状态
const globalStore = observable({
    isDev: _DEV_, // 开发模式
    isMobile: /Android|webOS|iPhone|iPad|BlackBerry|IEMobile|Nexus/i.test(navigator.userAgent), // 是否移动端
    browserInfo: getBrowserInfo(), //浏览器信息
    isIE: getBrowserInfo().browser === "IE",
    isFirefox: getBrowserInfo().browser === "Firefox",
    testValue1: 0, //  测试用的
    testValue2: 0, //  测试用的
    setTestVaue(v1: number, v2: number) {
        this.testValue1 = v1;
        this.testValue2 = v2;
    }
});

if (globalStore.isDev) {
    window.globalStore = globalStore;
}

export default globalStore;