'use strict';
import { ConfigProvider } from 'antd';
import 'antd/dist/antd.css';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import React from 'react';
import ReactDOM from 'react-dom';
import styles from './reset.less';
import Routers from './Routers';

const appDom = document.getElementById('app') as HTMLDivElement;

appDom.classList.add(styles.app, styles.scrollbar);

ReactDOM.render(
    <ConfigProvider locale={zhCN} >
        <Routers />
    </ConfigProvider>,
    appDom
);


if (_DEV_) {
    (module as any)['hot'] && (module as any)['hot'].accept();
}


if (!_DEV_ && "serviceWorker" in navigator) {
    window.addEventListener("load", () => {
        navigator.serviceWorker.register("./service-worker.js").then(registration => {
            // console.log("SW registered: ", registration);
        }).catch(registrationError => {
            // console.log("SW registration failed: ", registrationError);
        });
    });
}