import Btn1 from '@components/btn1/Btn1';
import ReactMixinComponent from '@ReactMixinComponent';
import React from 'react';
import styles from './error404.less';

export default class Error404 extends ReactMixinComponent<any, any> {

    goto(path: string) {
        this.mixin_history.push(path);
    }

    render() {
        return (
            <div className={styles.error}>
                <div className={styles.svg} />
                <div className={styles.content}>
                    <div className={styles.text}>
                        <h2>404</h2>
                        <p>抱歉，你访问的页面不存在</p>
                        <Btn1 onClick={() => this.goto('/')}> 返回首页 </Btn1>
                    </div>
                </div>
            </div >
        );
    }
}
