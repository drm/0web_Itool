/*
 * @Author: xiaosihan
 * @Date: 2021-03-28 18:47:55
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-07-10 20:05:36
 */


import DragTest from '@components/DragTest/DragTest';
import ReactComponentTs from '@components/ReactComponentTs/ReactComponentTs';
import * as React from 'react';
import styles from './Dev.less';

interface Props { }

interface State { }

export default class Dev extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className={styles.Dev}>
                <ReactComponentTs />
                <DragTest />
                <DragTest />
            </div>
        )
    }
}