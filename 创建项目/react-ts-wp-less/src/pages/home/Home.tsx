import globalStore from '@globalStore';
import ReactMixinComponent from '@ReactMixinComponent';
import React from 'react';
import styles from './home.less';
interface Iprops { }

interface Istate {
    isDev: boolean
}

export default class Home extends ReactMixinComponent<Iprops, Istate> {
    constructor(props: Iprops) {
        super(props);
    }

    state = {
        isDev: globalStore.isDev
    }

    componentDidMount() {
        this.mixin_autorun(() => {
            this.setState({
                isDev: globalStore.isDev
            });
        });
    }

    componentWillUnmount() { }

    render() {

        const { isDev } = this.state;

        return (
            <div className={styles.home}>
                DEV : {String(isDev)}
            </div>
        );
    }
}

