
// 页面总体布局
import ReactMixinComponent from '@ReactMixinComponent';
import React from 'react';
import Header from './hearder/Header';
import styles from './layout.less';

interface Iprops {

}

interface Istate {

}

export default class Layout extends ReactMixinComponent<Iprops, Istate> {
    constructor(props: Iprops) {
        super(props);
    }

    render() {
        return (
            <div className={styles.layout} >
                <Header />
                {this.props.children}
            </div>
        )
    }

}