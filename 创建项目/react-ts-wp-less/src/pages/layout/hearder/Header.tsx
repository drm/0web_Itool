
//头
import Btn1 from '@components/btn1/Btn1';
import { DevBtn } from '@components/DevBtn/DevBtn';
import globalStore from '@globalStore';
import ReactMixinComponent from '@ReactMixinComponent';
import utils from '@utils';
import worker from '@worker';
import React from 'react';
import styles from './header.less';

interface Iprops {
}

interface Istate {
}

export default class Header extends ReactMixinComponent<Iprops, Istate> {
    constructor(props: Iprops) {
        super(props);
    }

    componentDidMount(): void {
    }

    // 测试 mobxAutorun 生命周期
    modifyTestValue() {
        globalStore.setTestVaue(utils.rand(0, 100), utils.rand(0, 100));
    }

    goto(path: string) {
        this.mixin_history.push(path);
    }

    render() {
        return (
            <div className={styles.header}>
                <Btn1 onClick={() => this.goto('/home')}  >home</Btn1>

                <Btn1 onClick={() => this.goto('/dev')}  >dev</Btn1>

                <Btn1
                    onClick={async () => {
                        // 在第二线程中执行的方法测试
                        const res = await worker.test({ a: '1' });

                        console.log(res);
                    }}
                >
                    testWorker
                </Btn1>

                <Btn1 onClick={() => this.modifyTestValue()}>测试 mobxAutorun 生命周期</Btn1>

                <DevBtn />
            </div >
        );
    }
}