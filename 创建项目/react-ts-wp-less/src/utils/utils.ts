/*
 * @Author: xiaosihan
 * @Date: 2021-03-28 02:13:06
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-03-28 18:05:31
 */


// 全局公共方法
class Utils {
    constructor() { }

    // 数字加千分位
    thousandth(number: string | number) {
        number = String(number); // 转化成字符串
        while (number !== number.replace(/(\d)(\d{3})(\.|,|$)/, '$1,$2$3')) {
            number = number.replace(/(\d)(\d{3})(\.|,|$)/, '$1,$2$3');
        }
        return number;
    }

    // 取随机数在某个范围里
    rand(start: number, end: number) {
        return Math.floor(Math.random() * (end - start + 1) + start);
    }

}

const utils = new Utils();

export default utils;