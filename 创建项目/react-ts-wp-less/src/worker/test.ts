/*
 * @Author: 肖思汗 
 * @Date: 2020-03-18 15:56:47 
 * @Last Modified by: 肖思汗
 * @Last Modified time: 2020-03-18 16:38:45
 */

/**
 * @export
 * @param {*} jsonParam json格式的参数
 * @returns // 这个方法由第二个线程调用
 */
export default function test(jsonParam: { [key: string]: any }) {
    return {
        'type': '测试成功, 并返回传入的参数',
        ...jsonParam
    };
}