/*
 * @Author: 肖思汗 
 * @Date: 2020-07-30 23:27:11 
 * @Last Modified by: 肖思汗
 * @Last Modified time: 2020-07-30 23:50:57
 */
export type workMethodsParams = {
    data: {
        id: string,
        method: string;
        jsonParams: object;
    }
}

export type workMethodsReture = {
    data: {
        id: string;
        data: object;
    }
}