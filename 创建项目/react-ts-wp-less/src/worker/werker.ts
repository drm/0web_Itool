/*
 * @Author: 肖思汗 
 * @Date: 2020-03-18 15:31:34 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-04-06 23:07:32
 */

// 这里面的方法都在第二个线程中运行的
import cuid from 'cuid';

// @ts-ignore
import workThread from './workThread'; // 创建一个线程构造函数 webpack 需要配置 多线程加载器

import { workMethodsReture } from './type';

class work {

    constructor() {

        // 监听线程执行完成后的返回
        this.worker.addEventListener('message', (e: workMethodsReture) => {

            const { id, data } = e.data;

            this.resolves.get(id)(data); // 根据标识符执行回调

            this.resolves.delete(id); // 根据标识删除回调

        }, false);

    }

    worker = new workThread(); // 创建一个线程

    resolves = new Map(); // Promise回调函数管理

    call(method: string, jsonParams: {}): Promise<any> {
        return new Promise((resolve, reject) => {

            let id = cuid(); // 创建一个标识符

            // 在第二线程中 使用test方法传入 jsonParam参数
            this.worker.postMessage({ id, method, jsonParams });

            // 把 回调函数存到执行队列中
            this.resolves.set(id, resolve);

        });
    }

    test(jsonParams: {}): Promise<any> {
        return this.call('test', jsonParams);
    }

}

const worker = new work();

export default worker;