/*
 * @Author: 肖思汗 
 * @Date: 2019-08-19 21:14:20 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-04-06 23:05:13
 */
import test from './test' // 引入test方法

// 所有的方法集合
const methods: { [key: string]: any } = {
    test
}


self.addEventListener('message', (e) => {

    const { id, method, jsonParams } = e.data;

    let data = methods[method] && methods[method](jsonParams);

    // 返回标识符和计算结果
    // @ts-ignore
    self.postMessage({ id, data });

}, false);