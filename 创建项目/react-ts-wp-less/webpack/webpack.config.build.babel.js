/*
 * @Author: xiaosihan 
 * @Date: 2021-04-06 23:34:39 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-10-24 23:14:55
 */

import { GenerateSW } from 'workbox-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import webpack from 'webpack';
import webpackUtils from './webpackUtils';
import dayjs from 'dayjs';

const webpackBuildConf = {
    mode: 'production',
    devtool: 'eval',
    entry: {
        app: [
            'babel-polyfill', // js兼容处理 在.babelrc 中配置
            webpackUtils.indexPath
        ]
    },
    output: {
        publicPath: '',
        path: webpackUtils.buildPath,
        filename: 'js/build.[contenthash:4].js'
    },

    // 引用别名
    resolve: webpackUtils.resolve,

    module: {
        rules: [
            webpackUtils.cssLoader,// css 加载器
            webpackUtils.lessLoader, // less 加载器
            webpackUtils.jsLoader, // js 加载器
            webpackUtils.tsLoader, // ts 加载器
            webpackUtils.workerLoader, // 多线程加载器
            webpackUtils.flieLoader // 其他文件加载器
        ]
    },
    optimization: {
        minimize: true
    },
    performance: {
        hints: false // 每个文件是否限制大小
    },
    plugins: [
        new CleanWebpackPlugin({
            root: webpackUtils.buildPath  // 删除build
        }),
        new GenerateSW({
            clientsClaim: true,
            skipWaiting: true, // 安装成功后立即接管网站,注意这个要和 clientsClaim 一起设置为 true
            maximumFileSizeToCacheInBytes: Number.MAX_VALUE
        }),
        ...webpackUtils.plugins,
        new webpack.DefinePlugin({ // 全局变量定义
            _DEV_: false // 全局常量
        }),
        new HtmlWebpackPlugin({
            buildTime: dayjs().format("YYYY-MM-DD HH:mm:ss"),
            title: webpackUtils.indexTitle,
            favicon: webpackUtils.favicon, // 网站图标
            filename: 'index.html',
            template: webpackUtils.indexTemplate,
            inject: 'body',
            cache: false,
            compress: false,
            minify: {
                removeComments: true, //删除注释
                collapseWhitespace: false //删除空白符与换行符
            },
            meta: {
                viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'
            }
        })

    ]
}

export default webpackBuildConf;
