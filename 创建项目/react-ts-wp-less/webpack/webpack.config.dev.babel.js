/*
 * @Author: xiaosihan 
 * @Date: 2021-04-02 19:45:34 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-10-28 01:31:25
 */

import CaseSensitivePathsPlugin from 'case-sensitive-paths-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import internalIp from 'internal-ip';
import webpack from 'webpack';
import dayjs from 'dayjs';
import webpackUtils from './webpackUtils.js';

const webpackDevConf = {
    mode: 'development',
    devtool: 'eval-source-map', // ---------------在开发模式中 可以在浏览器中调试编译前的代码
    entry: {
        app: [
            'babel-polyfill', // ----------------------------------------js兼容处理 在.babelrc 中配置
            'react-hot-loader/patch', // --------------------------------开启React代码的模块热替换
            webpackUtils.indexPath
        ]
    },
    output: {
        publicPath: '',
        path: webpackUtils.buildPath,
        filename: 'js/[name].build.js'
    },

    // 引用别名
    resolve: webpackUtils.resolve,

    module: {
        rules: [
            webpackUtils.cssLoader, // css 加载器
            webpackUtils.lessLoader, // less 加载器
            webpackUtils.jsLoader, // js 加载器
            webpackUtils.tsLoader, // ts 加载器
            webpackUtils.workerLoader, // 多线程加载器
            webpackUtils.flieLoader // 其他文件加载器
        ]
    },
    optimization: {
        minimize: false
    },
    performance: {
        hints: false // 每个文件是否限制大小
    },
    cache: {
        type: 'filesystem'
    },
    devServer: {
        historyApiFallback: true, // 未找到文件时返回index.html文件
        host: '0.0.0.0',
        compress: true, // gzip 压缩
        open: false, // 启动时 打开浏览器访问项目
        port: webpackUtils.port,
		watchFiles: ['src/**/*'], // 'src/**/*.php'
        proxy: { // 代理
        }
    },
    plugins: [
        new webpack.DefinePlugin({ // 全局变量定义
            _DEV_: true // 全局常量
        }),
        new HtmlWebpackPlugin({
            buildTime: dayjs().format("YYYY-MM-DD HH:mm:ss"),
            title: webpackUtils.indexTitle,
            contentBase: '../src/asset/',
            dll: '', // 开发模式不用引入打包好的公用代码
            favicon: webpackUtils.favicon, // 网站图标
            filename: 'index.html',
            template: webpackUtils.indexTemplate,
            inject: 'body',
            cache: false,
            compress: false,
            meta: {
                viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'
            }
        }),
        new CaseSensitivePathsPlugin(), // 文件路径大小写敏感
        ...webpackUtils.plugins, // 公共的插件
        new class devServerReady {
            apply(compiler) {
                compiler.hooks.emit.tap('devServerReady', compilation => {
                    console.clear();
                    console.log('项目访问地址');
                    console.log(`http://localhost:${webpackUtils.port}`);
                    console.log(`http://${internalIp.v4.sync()}:${webpackUtils.port}`);
                })
            }
        }
    ]
};


export default webpackDevConf;