/*
 * @Author: xiaosihan
 * @Date: 2021-04-02 19:44:21
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2021-11-05 11:37:43
 */

import AntdDayjsWebpackPlugin from "antd-dayjs-webpack-plugin"; // antd 替换使用 dayjs
import MIniCssExtractPlugin from 'mini-css-extract-plugin';
import path from 'path';

/**
 * webpack 公共方法
 *
 * @class WebpackUtils
 */
class WebpackUtils {

    constructor() { }

    _path(dir) {
        return path.join(__dirname, dir);
    }

    // 端口
    port = '8080';

    //项目目录
    srcPath = this._path('../src');

    // 打包目录
    buildPath = this._path('../build');

    // 入口文件
    indexPath = this._path('../src/index.tsx');

    // 公共包的目录
    vendorPath = this._path('../vendor/')

    // 网页标题
    indexTitle = 'xsh-react-three';

    //网页图标
    favicon = this._path('../src/images/favicon.png');

    // html 文件
    indexTemplate = this._path('../src/index.html');

    // 引用别名
    resolve = {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.css', '.less', '".wasm"'],
        alias: {
            "@ReactMixinComponent": this._path("../src/ReactMixinComponent/ReactMixinComponent.tsx"),
            "@components": this._path("../src/components"),
            "@emiter": this._path("../src/emiter/emiter.ts"),
            "@images": this._path("../src/images"),
            "@pages": this._path("../src/pages"),
            "@fetch": this._path("../src/fetch"),
            "@globalStore": this._path("../src/globalStore.ts"),
            "@worker": this._path("../src/worker/werker.ts"),
            "@utils": this._path("../src/utils/utils.ts"),
        }
    };

    // js 加载器
    jsLoader = {
        test: /\.jsx?$/,
        use: ["babel-loader?cacheDirectory=true"]
    }

    //ts 加载器
    tsLoader = {
        test: /\.tsx?$/,
        exclude: this._path("../src/worker/workThread.ts"),
        use: ["ts-loader"]
    }

    // css 加载器
    cssLoader = {
        test: /\.css$/,
        use: [MIniCssExtractPlugin.loader, "css-loader", 'postcss-loader']
    }

    //less 加载器
    lessLoader = {
        test: /\.less$/,
        use: [
            "style-loader",
            {
                loader: 'css-loader',
                options: {
                    sourceMap: true,
                    modules: {
                        localIdentName: '[local]_[contenthash:2]',
                    }
                }
            },
            "postcss-loader",
            'less-loader'
        ],
    }

    // 文件加载器
    flieLoader = {
        test: /\.(png|jpg|jpeg|gif|svg|mp3|mp4|ogg|wav|obj|fbx|gltf|glb|woff|eot|ttf|otf|xlsx|xls|pdf)$/,
        use: [{
            loader: 'file-loader',
            options: {
                name: 'file/[name]_[contenthash:2].[ext]'
            }
        }]
    }

    // wasm 加载器
    wasmLoader = {
        test: /\.wasm$/,
        use: ["webassembly/experimental"],
    }

    // 多线程 加载器
    workerLoader = {
        test: /workThread\.ts$/,
        use: [{
            loader: 'worker-loader',
            options: {
                filename: 'worker/[name]_[contenthash:2].js'
            }
        },
        { loader: 'ts-loader' }
        ]
    }

    // 公共插件
    plugins = [
        new MIniCssExtractPlugin({     // 抽离css样式插件
            filename: 'index.[hash:8].css',    // 抽离后的文件名
        }),
        new AntdDayjsWebpackPlugin(), // dayjs 替代 momentjs
    ];

}

const webpackUtils = new WebpackUtils();

export default webpackUtils;