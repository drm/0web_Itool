@echo off
set "a=  "name": "react-ts-wp-less","
for /f "delims=" %%i in ('findstr /n .* %1\package.json') do (
    set "var=%%i"
    setlocal enabledelayedexpansion
    set var=!var:*:=!
    if "!var!" == "%a%" set var=!var:react-ts-wp-less=%2!
    (echo=!var!)>>%1\newjson.json
    endlocal
)
del %1\package.json
ren "%1\newjson.json" package.json
exit
