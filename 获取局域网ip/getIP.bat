@echo off 
mode con lines=1 cols=20

for /f "tokens=4" %%a in ('route print^|findstr 0.0.0.0.*0.0.0.0') do (
 set IP=%%a
)

echo %IP%>test.txt
clip < test.txt
del test.txt

mshta vbscript:msgbox("%IP% 已经复制到剪贴板",1,"局域网ip")(window.close)
exit
